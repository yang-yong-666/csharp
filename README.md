# CSharpTools

## 介绍
>C#工具项目,个人学习使用C#编写软件开发过程中用的工具类动态库，供所有人免费使用。
如有更好的建议可与作者联系交流
交流QQ：918546158

### 软件架构
>使用.NET Framework4.8架构。

### 内容简介

##### YYLogHelper
>对log4net封装简单的日志输出类

##### YYADONET
>提供对SQL Server访问的帮助类

## YYADONET
> 封装访问SQL Server帮助类
> 程序集：YYADONET；对象名称：SqlServerHelp

### 初始化
调用函数需要初始化数据库连接字符串（构造函数）
### 打开\关闭连接
##### 打开连接
>Open();
打开数据库方法（自动调用）
##### 关闭连接
>Close();
关闭数据库（执行返回DataReaderle方法时需要手动关闭）

### 更新方法
##### 函数名称
>NonQuery
更新数据库（增删改，多条语句默认进行事物处理）
+2个重载
##### 参数列表
>sql：要执行的sql语句（string）
para：参数列表（SqlParameter[]）

### 获取首行首列
##### 函数名称
>Scalar
执行SQL语句返回结果首行首列
+3重载
##### 参数列表
>sql：要执行的sql语句（string）
para：参数列表（SqlParameter[]）
exc：输出查询结果（object）
##### 返回值
>return：object（查询结果）&& int（执行结果标识 0成功、1失败）
### 获取DataReader数据
##### 函数
>DataReaderle
查询数据库获取DataReader数据集
+2重载
##### 参数列表
>sql：要执行的sql语句、存储过程名（string）
para：参数列表（SqlParameter[]）
cmdType：（可选，默认1）执行标志：1.语句；2.存储过程

### 获取DataSet数据
##### 函数名称
>DataSetle
查询数据库获取DataSet
+2重载
##### 参数列表
>sql：要执行的sql语句、存储过程名（string）
para：参数列表（SqlParameter[]）
cmdType：（可选，默认1）执行标志：1.语句；2.存储过程

### 数据表批量更新
##### 函数名称
>AddTable
将DataTable数据集批量插入指定数据表（此方法要求DataTable结构和数据表结构一致）
##### 参数列表
>dt：要插入的数据集（DataTable）
tableName：数据库表名称

## YYLogHelper
>封装log4net，提供日志输出到指定路径的方法，可通过配置文件更改日志输出设置；
##### 函数列表
>WriteLogInfo
WriteLogDebug
WriteLogWarn
WriteLogError
WriteLogFatal
+2重载
##### 参数列表
>info：输出说明（string）
se：错误说明（Exception）
##### log4net配置说明
略，代码中有详细注释

## ConfigDemo
> C#自定义配置文件的操作Demo，
> 使用介绍在以下文章：
> 博客园：<https://www.cnblogs.com/yangyongdashen-S/p/YiRenXiAn_CSharp_Config_1.html>
> CSDN：<https://blog.csdn.net/weixin_44312699/article/details/124524607?spm=1001.2014.3001.5501>

## YYIniHelper
> C#操作INI文件帮助类

#### IsHaveIniFileAndCreate函数
> 判断指定INI文件是否存在，不存在则创建
##### 参数列表
> fileName:string类型，INI文件路径
> isCreate:bool类型，可选参数，文件不存在时是否创建文件，默认false

#### WriteIniKeys函数
> 修改INI文件内容
##### 参数列表
> section:string类型，段落名称
> key：string类型，关键字名称
> value：string，值名称
> filePath：string类型，INI文件路径
> isCreate:bool类型，可选参数，文件不存在时是否创建文件，默认false

#### ReadIniKeys函数
> 获取INI文件内容 2重载
##### 参数列表
> section:string类型，段落名称
> key：string类型，关键字名称
> def：string类型，默认值
> value：string，值名称
> filePath：string类型，INI文件路径
> isCreate:bool类型，可选参数，文件不存在时是否创建文件，默认false

---
_个人交流QQ：1695690324_ <br/>
___原创不易，转载请注明出处___ <br/>
_博客园：<https://www.cnblogs.com/yangyongdashen-S/>_ <br/>
_CSDN：<https://blog.csdn.net/weixin_44312699?spm=1010.2135.3001.5343>_ <br/>
_Gitee：<https://gitee.com/yang-yong-666>_<br/>
_公众号：yi人夕岸_<br/>






