﻿/*
 =====================================================
 * Copyright(C)2022 YY
 * 文件名: GetConfigToDictionary.cs
 * 程序集: ConfigDemo
 * 文件功能: 提供调用.NET程序的自定义配置文件信息
 * 作者博客: https://www.cnblogs.com/yangyongdashen-S/
 * CSDN：https://blog.csdn.net/weixin_44312699?spm=1010.2135.3001.5343
 * Gitee：https://gitee.com/yang-yong-666
 * 公众号：yi人夕岸
 * 交流QQ: 1695690324
 * 邮箱: qq邮箱
 *
 * 说明：
 * 本项目提供功能有限，难免有考虑不到的情况，如发现错误或有什么建议，欢迎指正
 ======================================================
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigDemo
{
    /// <summary>
    /// 配置文件读取类，
    /// </summary>
    public static class GetConfigToDictionary
    {
        //ConfigurationSettings.GetConfig() 与ConfigurationManager.GetSection()两个方法都适用

        /// <summary>
        /// NameValueCollection
        /// </summary>
        public static Dictionary<string, string> NameValueConfigNode
        {
            get
            {
                NameValueCollection nvc = (NameValueCollection)ConfigurationManager.GetSection("NameValueConfigNode");
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (string key in nvc.AllKeys)
                {
                    result.Add(key, nvc[key]);
                }
                return result;
            }
        }

        /// <summary>
        /// Dictionary
        /// </summary>
        public static Dictionary<string, string> DictionaryConfigNode
        {
            get
            {
                IDictionary dict = (IDictionary)ConfigurationManager.GetSection("DictionaryConfigNode");
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (string key in dict.Keys)
                {
                    result.Add(key, dict[key].ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// SingleTag
        /// </summary>
        public static Dictionary<string, string> SingleTagConfigNode
        {
            get
            {
                Hashtable dict = (Hashtable)ConfigurationManager.GetSection("SingleTagConfigNode");
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (string key in dict.Keys)
                {
                    result.Add(key, dict[key].ToString());
                }
                return result;
            }
        }

        /// <summary>
        /// 自定义配置文件
        /// </summary>
        public static Dictionary<string, string> myConfig
        {
            get
            {
                NameValueCollection config = ConfigurationManager.GetSection("MyConfigData") as NameValueCollection;
                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (string key in config.AllKeys)
                {
                    result.Add(key, config[key]);
                }
                return result;
            }
        }




    }
}
