﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogHelper
{

    public class loghelp
    {
        public static readonly log4net.ILog logInfo = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logInfoException = log4net.LogManager.GetLogger("logerror");
        public static readonly log4net.ILog logDebug = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logWarn = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logError = log4net.LogManager.GetLogger("loginfo");
        public static readonly log4net.ILog logFatal = log4net.LogManager.GetLogger("loginfo");

        public static void WriteLog(string info)
        {

            if (logInfo.IsInfoEnabled)
            {
                logInfo.Info(info);
            }
        }

        public static void WriteLog(string info, Exception se)
        {
            if (logInfoException.IsErrorEnabled)
            {
                logInfoException.Info(info, se);
            }
        }

    }
}
