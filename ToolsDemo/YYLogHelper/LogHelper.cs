﻿/*
 =====================================================
 * Copyright(C)2021 YY
 * 文件名: LogHelper.cs
 * 程序集: YYLogHelper
 * 文件功能: 提供对log4net的引用，实现日志记录功能。
 * 作者博客: https://www.cnblogs.com/yangyongdashen-S/
 * CSDN：https://blog.csdn.net/weixin_44312699?spm=1010.2135.3001.5343
 * Gitee：https://gitee.com/yang-yong-666
 * 公众号：yi人夕岸
 * 交流QQ: 1695690324
 * 邮箱: qq邮箱
 *
 * 说明：
 * 本项目提供功能有限，难免有考虑不到的情况，如发现错误或有什么建议，欢迎指正
 ======================================================
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YYLogHelper
{
    /// <summary>
    /// 日志帮助类
    /// </summary>
    public class LogHelper
    {
        private static readonly log4net.ILog logInfo = log4net.LogManager.GetLogger("loginfo");
        private static readonly log4net.ILog logInfoException = log4net.LogManager.GetLogger("loginfo");
        private static readonly log4net.ILog logDebug = log4net.LogManager.GetLogger("logdebug");
        private static readonly log4net.ILog logDebugException = log4net.LogManager.GetLogger("logdebug");
        private static readonly log4net.ILog logWarn = log4net.LogManager.GetLogger("logwarn");
        private static readonly log4net.ILog logWarnException = log4net.LogManager.GetLogger("logwarn");
        private static readonly log4net.ILog logError = log4net.LogManager.GetLogger("logerror");
        private static readonly log4net.ILog logErrorException = log4net.LogManager.GetLogger("logerror");
        private static readonly log4net.ILog logFatal = log4net.LogManager.GetLogger("logfatal");
        private static readonly log4net.ILog logFatalException = log4net.LogManager.GetLogger("logfatal");

        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="info">日志信息</param>
        public static void WriteLogInfo(string info)
        {

            if (logInfo.IsInfoEnabled)
            {
                logInfo.Info(info);
            }
        }
        /// <summary>
        /// 普通日志输出异常
        /// </summary>
        /// <param name="info">日志信息</param>
        /// <param name="se">异常信息</param>
        public static void WriteLogInfo(string info, Exception se)
        {
            if (logInfoException.IsErrorEnabled)
            {
                logInfoException.Info(info, se);
            }
        }
        public static void WriteLogDebug(string info)
        {

            if (logDebug.IsInfoEnabled)
            {
                logDebug.Debug(info);
            }
        }
        public static void WriteLogDebug(string info, Exception se)
        {
            if (logDebugException.IsErrorEnabled)
            {
                logDebugException.Debug(info, se);
            }
        }
        public static void WriteLogWarn(string info)
        {

            if (logWarn.IsInfoEnabled)
            {
                logWarn.Warn(info);
            }
        }

        public static void WriteLogWarn(string info, Exception se)
        {
            if (logWarnException.IsErrorEnabled)
            {
                logWarnException.Warn(info, se);
            }
        }
        public static void WriteLogError(string info)
        {

            if (logError.IsInfoEnabled)
            {
                logError.Error(info);
            }
        }
        public static void WriteLogError(string info, Exception se)
        {
            if (logErrorException.IsErrorEnabled)
            {
                logErrorException.Error(info, se);
            }
        }
        public static void WriteLogFatal(string info)
        {

            if (logFatal.IsInfoEnabled)
            {
                logFatal.Fatal(info);
            }
        }
        public static void WriteLogFatal(string info, Exception se)
        {
            if (logFatalException.IsErrorEnabled)
            {
                logFatalException.Fatal(info, se);
            }
        }

    }
}
