﻿
using ConfigDemo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using YYADONET;
using YYEnAndDecryption;
using static System.Net.Mime.MediaTypeNames;

namespace ToolsDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region 调用SqlServer

            SqlConnection conn = new SqlConnection("Data Source=127.0.0.1;Initial Catalog=Seesion2;Persist Security Info=True;Uid=sa;Pwd=46900@yangyong.22");
            conn.Open();
            SqlCommand cmd = new SqlCommand();  
            cmd.CommandText = "dfss";
            cmd.Connection = conn;
            var test = cmd.ExecuteNonQuery(); 
            cmd.Dispose();


            SqlServerHelp sqlServerHelp = new SqlServerHelp("Data Source=127.0.0.1;Initial Catalog=Seesion2;Persist Security Info=True;User ID=sa;Password=46900@yangyong.22");
            sqlServerHelp.NonQuery("sql语句");
            #endregion

            #region 调用LogHelp
            //YYLogHelper.LogHelper.WriteLogInfo("000", new Exception("222"));
            //YYLogHelper.LogHelper.WriteLogDebug("000", new Exception("222"));
            //YYLogHelper.LogHelper.WriteLogWarn("000", new Exception("222"));
            //YYLogHelper.LogHelper.WriteLogError("000", new Exception("222"));
            //YYLogHelper.LogHelper.WriteLogFatal("000", new Exception("222"));
            #endregion

            #region 调用自定义配置文件
            //Dictionary<string, string> config = new Dictionary<string, string>();
            //Console.WriteLine("=======NameValueConfigNode========");
            //config = GetConfigToDictionary.NameValueConfigNode;
            //foreach (KeyValuePair<string, string> key in config)
            //{
            //    Console.WriteLine(key.Key + "：" + key.Value);
            //}
            //Console.WriteLine("");

            //Console.WriteLine("=======DictionaryConfigNode========");
            //config = GetConfigToDictionary.DictionaryConfigNode;
            //foreach (KeyValuePair<string, string> key in config)
            //{
            //    Console.WriteLine(key.Key + "：" + key.Value);
            //}
            //Console.WriteLine("");

            //Console.WriteLine("=======SingleTagConfigNode========");
            //config = GetConfigToDictionary.SingleTagConfigNode;
            //foreach (KeyValuePair<string, string> key in config)
            //{
            //    Console.WriteLine(key.Key + "：" + key.Value);
            //}
            //Console.WriteLine("");

            //Console.WriteLine("=======自定义配置文件1（指定路径）========");
            //config = GetConfigToDictionary.myConfig;
            //foreach (KeyValuePair<string, string> key in config)
            //{
            //    Console.WriteLine(key.Key + "：" + key.Value);
            //}
            //Console.WriteLine("");

            //Console.WriteLine("===自定义配置文件2（配置文件分组）===");
            //config = GetConfigToDictionary.myConfig;
            //foreach (KeyValuePair<string, string> key in config)
            //{
            //    Console.WriteLine(key.Key + "：" + key.Value);
            //}
            //Console.WriteLine("");

            #endregion

            #region 调通INI帮助类
            //C:\Users\YangYong\Desktop\test.ini
            //查看文件是否存在并创建
            //YYIniHelper.INIUtility.IsHaveIniFileAndCreate(@"D:\0rs\test", false);
            //写入文件内容
            //YYIniHelper.INIHelper.WriteIniKeys("S1", null, null, @"C:\test.ini");
            //string sss = YYIniHelper.INIHelper.ReadIniKeys("S1", "k1", @"C:\test.ini");


            #endregion

            #region MD5
            MD5Decryption.MD5EncryptFile(@"C:\Users\YangYong\Desktop\html生日快乐源代码_v1.1.7z");
            #endregion

            Console.ReadKey();
        }
    }
}
